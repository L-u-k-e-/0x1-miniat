<<< 

We would expect the following execution sequence from the code below.
The "PCQ" column depicts the expected contents of the PC queue right *before* the fetch stage of the current line. 

PCQ         F    D    E    W
-----------------------------
A           A        
B           B    A        
C           C    B    A    
F E D       D    C    B    A
H G F E     E    D    C    B
H G F       F    E    D    C
H G         G    F    E    D
H           H    G    F    E
I           I    H    G    F
J           J    I    H    G
K           K    J    I    H
L           L    K    J    I
                 L    K    J
                      L    K
                           L

So, the registers should spell "CAKE". mmmm CAKE.

>>>                             


.mode NOP_DELAY off
.address 0x2000

!main
	movi pc = !A          # A
	movi pc = !C          # B
	bra [!B]              # C
	bra [!D]              # D

!A
	movi r1 = 'C'         # E      
!B 
	movi r2 = 'A'         # F                                  
!C
	movi r3 = 'K'         # G
!D 
	movi r4 = 'E'         # H

!done
	NOP                   # I
	NOP                   # J
	NOP                   # K
	movi r100 = 1         # L
	NOP
	NOP
	NOP