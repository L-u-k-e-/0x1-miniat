<<<
	These are series of tests for the INT instruction under various conditions. 

	TEST1		
		Simple INT, then IRET with a low priority and a high priority separately. 
	
	TEST2		
		Interrupt a low priority running interrupt with a higer priority one. 
		
	TEST3		
		Try to interrupt a high priority running int with a lower priority one.  
		
	TEST4
		Try to trigger an interrupt handler for which the corresponding flag register is disabled

	TEST5
		Enable a flagged (previously disabled) high priority intterrupt in the middle of handling
		a lower priority interrupt. 
>>>


.const IVT_6           0x00001AE0 # Timer 0 interrupt vector address
.const IVT_7           0x00001AE1 # Timer 1 interrupt vector address
.const IVT_8           0x00001AE2 # Timer 2 interrupt vector address
.const IVT_9           0x00001AE3 # Timer 3 interrupt vector address

.const IVT_32          0x00001AFA # External interrupt 0 interrupt vector address
.const IVT_33          0x00001AFB # External interrupt 1 interrupt vector address
.const IVT_34          0x00001AFC # External interrupt 2 interrupt vector address
.const IVT_35          0x00001AFD # External interrupt 3 interrupt vector address
.const IVT_36          0x00001AFE # External interrupt 4 interrupt vector address
.const IVT_37          0x00001AFF # External interrupt 5 interrupt vector address
.const IVT_38          0x00001B00 # External interrupt 6 interrupt vector address
.const IVT_39          0x00001B01 # External interrupt 7 interrupt vector address
.const IVT_40          0x00001B02 # External interrupt 8 interrupt vector address
.const IVT_41          0x00001B03 # External interrupt 9 interrupt vector address

.const IER_A           0x00001F1A # Interrupt enable register for low word 
.const IER_B           0x00001F1B # Interrupt enable register for high word 

.const IFR_A           0x00001F1C # Interrupt flag register for low word
.const IFR_B           0x00001F1D # Interrupt flag register for high word

.const SYS_REG         0x00001B1E # Global interrupt enable register
.const SYS_REG_IE_MASK 0x00000001 


.address 0x2000





!setup
# Enable all individual interrupts	
	MOVI r1 = 0xFFFFFFFF
	STOR [IER_A] = r1
	STOR [IER_B] = r1

#Enable interrups globally
	MOVI r1 = SYS_REG_IE_MASK
	STOR [SYS_REG] = r1

  movi r5 = 0  # r5 will be incremented when we're beginning a new test
  movi r6 = 0  # r6 will be set to 1 when we're done with the current test





<<<
	TEST1: Basic interrupt using the INT instruction. 
>>>

!test1
	MOVI r5 = 1
	MOVI r1 = !test1_int
	STOR [IVT_6] = r1        
	INT 6

	MOVI r6  = 1
	BRA [!test1_exit]

!test1_int
	MOVI r51 = 1
	IRET

!test1_exit
	MOVI r5  = 0 
	MOVI r51 = 0
	MOVI r6  = 0 
	NOP
	NOP
	NOP
	BRA [!test2]




<<<
	TEST2: Interrupt a low priority interrupt with a higher priority one. 
>>>
!test2
	MOVI r5 = 2
	MOVI r1 = !test2_int_low_priority
	STOR [IVT_7] = r1        
	INT 7

	MOVI r6  = 1
	BRA [!test2_exit]

!test2_int_low_priority
	MOVI r1 = !test2_int_high_priority
	STOR [IVT_6] = r1    
	INT 6
	MOVI r52 = 1
	IRET

!test2_int_high_priority
	MOVI r51 = 1
	IRET

!test2_exit
	MOVI r5  = 0 
	MOVI r51 = 0
	MOVI r52 = 0
	MOVI r6  = 0 
	NOP
	NOP
	NOP
	BRA [!test3]







<<<
	TEST3: Attempt to interrupt a high priority interrupt with a lower priority one. 
>>>
!test3
	MOVI r5 = 3
	MOVI r1 = !test3_int_high_priority
	STOR [IVT_6] = r1        
	INT 6

	MOVI r6  = 1
	BRA [!test3_exit]

!test3_int_high_priority
	MOVI r1 = !test3_int_low_priority
	STOR [IVT_7] = r1    
	INT 7
	MOVI r51 = 1
	IRET

!test3_int_low_priority
	MOVI r52 = 1
	IRET

!test3_exit
	MOVI r5  = 0 
	MOVI r51 = 0
	MOVI r52 = 0
	MOVI r6  = 0 
	NOP
	NOP
	NOP
	BRA [!test4]







<<<
	TEST4: Try to trigger an interrupt handler for which the corresponding flag register is disabled
>>>
!test4
	MOVI r1 = 0x00000000
	STOR [IER_A] = r1
	
	MOVI r5 = 4
	MOVI r1 = !test4_int
	STOR [IVT_6] = r1        
	INT 6

	MOVI r6  = 1
	BRA [!test4_exit]

!test4_int
	MOVI r51 = 1
	IRET

!test4_exit
	MOVI r5  = 0 
	MOVI r51 = 0
	MOVI r6  = 0 
	NOP
	NOP
	NOP
	BRA [!test5]





<<<
	TEST5: Enable a flagged (previously disabled) high priority intterrupt while in the midst of handling a lower priority interrupt. 
>>>
!test5
	MOVI r1 = 0x00000080
	STOR [IER_A] = r1

	MOVI r5 = 5
	MOVI r1 = !test5_int_high_priority
	STOR [IVT_6] = r1
	INT 6 # currently disabled

	MOVI r1 = !test5_int_low_priority
	STOR [IVT_7] = r1        
	INT 7 # this one's enabled

	MOVI r6  = 1
	BRA [!test5_exit]

!test5_int_low_priority
	MOVI r51 = 1
	MOVI r1 = 0xFFFFFFFF
	STOR [IER_A] = r1 # enable INT 6 (and keep INT 7 Enabled)

	MOVI r53 = 1
	IRET

!test5_int_high_priority
	MOVI r52 = 1
	IRET

!test5_exit
	MOVI r5  = 0 
	MOVI r51 = 0
	MOVI r52 = 0
	MOVI r53 = 0
	MOVI r6  = 0 
	NOP
	NOP
	NOP
	BRA [!test6]




!test6
	BRA [!test6]
