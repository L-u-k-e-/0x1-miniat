#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "miniat_priv_typedefs.h"
#include "miniat_priv_defines.h"
#include "miniat_priv_structures.h"
#include "miniat_priv_pc.h"

#include "miniat/miniat.h"

#include "CuTest.h"



void t_pc___add_last(CuTest *tc) {

	miniat m;
	m_uword data;

	memset(&m, 0, sizeof(miniat));

	m_pc_init(&m);

	data = 1;
	m_pc_add_last(&m, data);
	CuAssert(tc, "PC queue size equals 1", m.pc.size == 1);
	CuAssert(tc, "PC queue 1 equals 1", m.pc.queue[0] == 1);

	data = 2;
	m_pc_add_last(&m, data);
	CuAssert(tc, "PC queue size equals 2", m.pc.size == 2);
	CuAssert(tc, "PC queue position 1 equals 1", m.pc.queue[0] == 1);
	CuAssert(tc, "PC queue position 2 equals 2", m.pc.queue[1] == 2);

	data = 3;
	m_pc_add_last(&m, data);
	CuAssert(tc, "PC queue size equals 3", m.pc.size == 3);
	CuAssert(tc, "PC queue position 1 equals 1", m.pc.queue[0] == 1);
	CuAssert(tc, "PC queue position 2 equals 2", m.pc.queue[1] == 2);
	CuAssert(tc, "PC queue position 3 equals 3", m.pc.queue[2] == 3);

	data = 4;
	m_pc_add_last(&m, data);
	CuAssert(tc, "PC queue size equals 4", m.pc.size == 4);
	CuAssert(tc, "PC queue position 1 equals 1", m.pc.queue[0] == 1);
	CuAssert(tc, "PC queue position 2 equals 2", m.pc.queue[1] == 2);
	CuAssert(tc, "PC queue position 3 equals 3", m.pc.queue[2] == 3);
  CuAssert(tc, "PC queue position 4 equals 4", m.pc.queue[3] == 4);


	return;
}




void t_pc___remove_first(CuTest *tc) {

	miniat m;
	m_uword data;

	memset(&m, 0, sizeof(miniat));

	m_pc_init(&m);

	data = 1;
	m_pc_add_last(&m, data);
	data = 2;
	m_pc_add_last(&m, data);
	data = 3;
	m_pc_add_last(&m, data);
	data = 4;
	m_pc_add_last(&m, data);

	CuAssert(tc, "PC queue size equals 4", m.pc.size == 4);
	CuAssert(tc, "PC queue position 1 equals 1", m.pc.queue[0] == 1);
	CuAssert(tc, "PC queue position 2 equals 2", m.pc.queue[1] == 2);
	CuAssert(tc, "PC queue position 3 equals 3", m.pc.queue[2] == 3);
	CuAssert(tc, "PC queue position 4 equals 4", m.pc.queue[3] == 4);


  m_pc_remove_first(&m);
	CuAssert(tc, "PC queue size equals 3", m.pc.size == 3);
	CuAssert(tc, "PC queue position 1 equals 2", m.pc.queue[0] == 2);
	CuAssert(tc, "PC queue position 2 equals 3", m.pc.queue[1] == 3);
	CuAssert(tc, "PC queue position 3 equals 4", m.pc.queue[2] == 4);

	m_pc_remove_first(&m);
	CuAssert(tc, "PC queue size equals 2", m.pc.size == 2);
	CuAssert(tc, "PC queue position 1 equals 3", m.pc.queue[0] == 3);
	CuAssert(tc, "PC queue position 2 equals 4", m.pc.queue[1] == 4);

	m_pc_remove_first(&m);
	CuAssert(tc, "PC queue size equals 1", m.pc.size == 1);
	CuAssert(tc, "PC queue position 1 equals 4", m.pc.queue[0] == 4);

	m_pc_remove_first(&m);
	CuAssert(tc, "PC queue size equals 0", m.pc.size == 0);

	return;
}




void t_pc___clear(CuTest *tc) {

	miniat m;
	m_uword data;

	memset(&m, 0, sizeof(miniat));

	m_pc_init(&m);

	data = 1;
	m_pc_add_last(&m, data);
	
	data = 2;
	m_pc_add_last(&m, data);

	data = 3;
	m_pc_add_last(&m, data);

	data = 4;
	m_pc_add_last(&m, data);

	m_pc_clear(&m);
	
	CuAssert(tc, "PC queue size equals 0", m.pc.size == 0);

	return;
}
