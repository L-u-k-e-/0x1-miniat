#include <stdio.h>
#include <stdlib.h>

#include "miniat/miniat.h"
#include "miniat_priv_registers.h"
#include "miniat_priv_defines.h"
#include "miniat_priv_structures.h"
#include "miniat_priv_hazards.h"

#include "CuTest.h"



int spin (miniat *m, int reg, int val) {
	int cycles = 0;
	while(m->reg[reg] != val && cycles < 1000) { 
		miniat_clock(m);
		cycles++;
	}
	return cycles;
}



void t_exec___interrupts (CuTest *tc) {

	FILE *bin_file;
	miniat *miniat;
	m_uword data;
	int cycles = 0;
	int relative_cycles = 0;
	const char *log_name = __func__;

	char *path = OUTDIR"/bin/interrupts.bin";
	bin_file = fopen(path, "rb");
	CuAssert(tc, "Should not be NULL", bin_file != NULL);

	miniat = miniat_new (bin_file);
	CuAssert(tc, "Should not be NULL", miniat != NULL);




	/*
	 *	TEST1: Basic interrupt
	 */
	spin(miniat, 5, 1);
	CuAssert(tc, "Register 5 should be 1",  miniat->reg[5]  == 1); 
	CuAssert(tc, "Register 6 should be 0",  miniat->reg[6]  == 0); 
	CuAssert(tc, "Register 51 should be 0", miniat->reg[51] == 0); 
	spin(miniat, 51, 1);
	CuAssert(tc, "Register 5 should be 1",  miniat->reg[5]  == 1); 
	CuAssert(tc, "Register 6 should be 0",  miniat->reg[6]  == 0); 
	CuAssert(tc, "Register 51 should be 1", miniat->reg[51] == 1);
	spin(miniat, 6, 1); 
	CuAssert(tc, "Register 5 should be 1",  miniat->reg[5]  == 1); 
	CuAssert(tc, "Register 6 should be 1",  miniat->reg[6]  == 1); 
	CuAssert(tc, "Register 51 should be 1", miniat->reg[51] == 1); 



	/*
	 * TEST2: Interrupt a low priority with a higher priority 
	 */
  spin(miniat, 5, 2);
	CuAssert(tc, "Register 5 should be 2",  miniat->reg[5]  == 2); 
	CuAssert(tc, "Register 6 should be 0",  miniat->reg[6]  == 0); 
	CuAssert(tc, "Register 51 should be 0", miniat->reg[51] == 0); 
	CuAssert(tc, "Register 52 should be 0", miniat->reg[52] == 0); 
	spin(miniat, 51, 1);
	CuAssert(tc, "Register 5 should be 2",  miniat->reg[5]  == 2); 
	CuAssert(tc, "Register 6 should be 0",  miniat->reg[6]  == 0); 
	CuAssert(tc, "Register 51 should be 1", miniat->reg[51] == 1); 
	CuAssert(tc, "Register 52 should be 0", miniat->reg[52] == 0); 
	spin(miniat, 52, 1);
	CuAssert(tc, "Register 5 should be 2",  miniat->reg[5]  == 2); 
	CuAssert(tc, "Register 6 should be 0",  miniat->reg[6]  == 0); 
	CuAssert(tc, "Register 51 should be 1", miniat->reg[51] == 1); 
	CuAssert(tc, "Register 52 should be 1", miniat->reg[52] == 1); 
	spin(miniat, 6, 1);
	CuAssert(tc, "Register 5 should be 2",  miniat->reg[5]  == 2); 
	CuAssert(tc, "Register 6 should be 1",  miniat->reg[6]  == 1); 
	CuAssert(tc, "Register 51 should be 1", miniat->reg[51] == 1); 
	CuAssert(tc, "Register 52 should be 1", miniat->reg[52] == 1);



	/*
	 * TEST3: Attempt to interrupt a high priority with a lower priority 
	 */
  spin(miniat, 5, 3);
	CuAssert(tc, "Register 5 should be 3",  miniat->reg[5]  == 3); 
	CuAssert(tc, "Register 6 should be 0",  miniat->reg[6]  == 0); 
	CuAssert(tc, "Register 51 should be 0", miniat->reg[51] == 0); 
	CuAssert(tc, "Register 52 should be 0", miniat->reg[52] == 0); 
	spin(miniat, 51, 1);
	CuAssert(tc, "Register 5 should be 3",  miniat->reg[5]  == 3); 
	CuAssert(tc, "Register 6 should be 0",  miniat->reg[6]  == 0); 
	CuAssert(tc, "Register 51 should be 1", miniat->reg[51] == 1); 
	CuAssert(tc, "Register 52 should be 0", miniat->reg[52] == 0); 
	spin(miniat, 52, 1);
	CuAssert(tc, "Register 5 should be 3",  miniat->reg[5]  == 3); 
	CuAssert(tc, "Register 6 should be 0",  miniat->reg[6]  == 0); 
	CuAssert(tc, "Register 51 should be 1", miniat->reg[51] == 1); 
	CuAssert(tc, "Register 52 should be 1", miniat->reg[52] == 1); 
	spin(miniat, 6, 1);
	CuAssert(tc, "Register 5 should be 3",  miniat->reg[5]  == 3); 
	CuAssert(tc, "Register 6 should be 1",  miniat->reg[6]  == 1); 
	CuAssert(tc, "Register 51 should be 1", miniat->reg[51] == 1); 
	CuAssert(tc, "Register 52 should be 1", miniat->reg[52] == 1);  


	/*
	 *	TEST4: Try to trigger an interrupt handler for which the corresponding flag register is disabled
	 */
	spin(miniat, 5, 4);
	CuAssert(tc, "Register 5 should be 4",  miniat->reg[5]  == 4); 
	CuAssert(tc, "Register 6 should be 0",  miniat->reg[6]  == 0); 
	CuAssert(tc, "Register 51 should be 0", miniat->reg[51] == 0); 
	spin(miniat, 6, 1);
	CuAssert(tc, "Register 5 should be 4",  miniat->reg[5]  == 4); 
	CuAssert(tc, "Register 6 should be 1",  miniat->reg[6]  == 1); 
	CuAssert(tc, "Register 51 should be 0", miniat->reg[51] == 0);


	/*
	 * TEST5: Enable a flagged (previously disabled) high priority intterrupt while in the midst of 
	 * handling a lower priority interrupt. 
   */ 
	spin(miniat, 5, 5);
	CuAssert(tc, "Register 5 should be 5",  miniat->reg[5]  == 5); 
	CuAssert(tc, "Register 6 should be 0",  miniat->reg[6]  == 0); 
	CuAssert(tc, "Register 51 should be 0", miniat->reg[51] == 0); 
	CuAssert(tc, "Register 52 should be 0", miniat->reg[52] == 0); 
	CuAssert(tc, "Register 53 should be 0", miniat->reg[53] == 0); 
	spin(miniat, 51, 1);
	CuAssert(tc, "Register 5 should be 5",  miniat->reg[5]  == 5); 
	CuAssert(tc, "Register 6 should be 0",  miniat->reg[6]  == 0); 
	CuAssert(tc, "Register 51 should be 1", miniat->reg[51] == 1); 
	CuAssert(tc, "Register 52 should be 0", miniat->reg[52] == 0); 
	CuAssert(tc, "Register 53 should be 0", miniat->reg[53] == 0); 
	spin(miniat, 52, 1);
	CuAssert(tc, "Register 5 should be 5",  miniat->reg[5]  == 5); 
	CuAssert(tc, "Register 6 should be 0",  miniat->reg[6]  == 0); 
	CuAssert(tc, "Register 51 should be 1", miniat->reg[51] == 1); 
	CuAssert(tc, "Register 52 should be 1", miniat->reg[52] == 1); 
	CuAssert(tc, "Register 53 should be 0", miniat->reg[53] == 0);
	spin(miniat, 53, 1);
	CuAssert(tc, "Register 5 should be 5",  miniat->reg[5]  == 5); 
	CuAssert(tc, "Register 6 should be 0",  miniat->reg[6]  == 0); 
	CuAssert(tc, "Register 51 should be 1", miniat->reg[51] == 1); 
	CuAssert(tc, "Register 52 should be 1", miniat->reg[52] == 1); 
	CuAssert(tc, "Register 53 should be 1", miniat->reg[53] == 1);  
	spin(miniat, 6, 1);
	CuAssert(tc, "Register 5 should be 5",  miniat->reg[5]  == 5); 
	CuAssert(tc, "Register 6 should be 1",  miniat->reg[6]  == 1); 
	CuAssert(tc, "Register 51 should be 1", miniat->reg[51] == 1); 
	CuAssert(tc, "Register 52 should be 1", miniat->reg[52] == 1);  
	CuAssert(tc, "Register 53 should be 1", miniat->reg[53] == 1);




	 
	miniat_free(miniat);
	return;
}
