#include <stdio.h>
#include <stdlib.h>

#include "miniat/miniat.h"
#include "miniat_priv_registers.h"
#include "miniat_priv_defines.h"
#include "miniat_priv_structures.h"
#include "miniat_priv_hazards.h"

#include "CuTest.h"

void t_exec___flush (CuTest *tc) {

	FILE *bin_file;
	miniat *miniat;
	m_uword data;
	int cycles = 0;
	const char *log_name = __func__;

	/* Read in the binary testing file and instantiate a miniat. */
	char *path = OUTDIR"/bin/flush.bin";
	bin_file = fopen(path, "rb");
	CuAssert(tc, "Should not be NULL", bin_file != NULL);

	miniat = miniat_new (bin_file);
	CuAssert(tc, "Should not be NULL", miniat != NULL);

	/* 
	 * Starting values:
	 *   r1: 'B'
	 *   r2: 'n'
	 *   r3: 'p'
	 *   r4: 't'
	 *
	 * Expected ending values:
	 *   r1: 'A'
	 *   r2: 'm'
	 *   r3: 'o'
	 *   r4: 's'
	 */

	/* Process instructions until we reach the end of section 1. */
	while(miniat->reg[100] != 1) { 
		miniat_clock(miniat);
		cycles++;
	}

	CuAssert(tc, "Register 1 should be 'A'", miniat->reg[1] == 'A'); 
	CuAssert(tc, "Register 2 should be 'm'", miniat->reg[2] == 'm'); 
	CuAssert(tc, "Register 3 should be 'o'", miniat->reg[3] == 'o'); 
	CuAssert(tc, "Register 4 should be 's'", miniat->reg[4] == 's'); 

	CuAssertIntEquals(tc, "Should have completed 15 cycles", 15, cycles); 


	/* Process instructions until we reach the end of section 2. */
	while(miniat->reg[100] != 2) { 
		miniat_clock(miniat); 
		cycles++;
	}

	CuAssert(tc, "Register 1 should be 'A'", miniat->reg[1] == 'A'); 
	CuAssert(tc, "Register 2 should be 'm'", miniat->reg[2] == 'm'); 
	CuAssert(tc, "Register 3 should be 'o'", miniat->reg[3] == 'o'); 
	CuAssert(tc, "Register 4 should be 's'", miniat->reg[4] == 's'); 

	CuAssertIntEquals(tc, "Should have completed 27 cycles", 27, cycles); 

	miniat_free(miniat);

	return;
}
