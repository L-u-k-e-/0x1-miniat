#include <stdio.h>
#include <stdlib.h>

#include "miniat/miniat.h"
#include "miniat_priv_registers.h"
#include "miniat_priv_defines.h"
#include "miniat_priv_structures.h"
#include "miniat_priv_hazards.h"

#include "CuTest.h"

void t_exec___pc_queue (CuTest *tc) {

	FILE *bin_file;
	miniat *miniat;
	m_uword data;
	int cycles = 0;
	const char *log_name = __func__;

	char *path = OUTDIR"/bin/pc_queue.bin";
	bin_file = fopen(path, "rb");
	CuAssert(tc, "Should not be NULL", bin_file != NULL);

	miniat = miniat_new (bin_file);
	CuAssert(tc, "Should not be NULL", miniat != NULL);

	while(miniat->reg[100] != 1 && cycles < 100) { 
		miniat_clock(miniat);
		cycles++;
	}

	CuAssert(tc, "Register 1 should be 'C'", miniat->reg[1] == 'C'); 
	CuAssert(tc, "Register 2 should be 'A'", miniat->reg[2] == 'A'); 
	CuAssert(tc, "Register 3 should be 'K'", miniat->reg[3] == 'K'); 
	CuAssert(tc, "Register 4 should be 'E'", miniat->reg[4] == 'E'); 

	miniat_free(miniat);
	return;
}
