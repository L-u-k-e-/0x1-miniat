#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

#include "miniat/miniat.h"
#include "miniat_priv_util.h"

/*
 * Provides a deep copy of the buffer passed in, NULL buffer gives NULL back.
 */
char *m_util_str_duplicate(miniat *m, char *s) {

	char *result = NULL;
	if(s && m) {
		result = malloc(sizeof(char) * (strlen(s) + 1));
		if(!result) {
			THROW(m_error_num_out_of_mem);
		}
		else {
			strcpy(result, s);
		}
	}

	return result;
}

/**
 * Find the lowest order set bit (if any) in a 32 bit value.
 * @note
 *  Big O Log(n) -- Backed by binary bitwise search
 *
 * @param bits
 *  The 32 bit value to scan
 * @return
 *  The bit position of the lowest set bit.  -1 if no bits
 */
int m_util_priority_scan_lowest(m_uword bits) {

	int n;

	if(bits == 0) {
		return -1;
	}

	n = 0;

	if(!(bits & 0x0000FFFF)) { n += 16; bits >>= 16; }
	if(!(bits & 0x000000FF)) { n +=  8; bits >>=  8; }
	if(!(bits & 0x0000000F)) { n +=  4; bits >>=  4; }
	if(!(bits & 0x00000003)) { n +=  2; bits >>=  2; }
	if(!(bits & 0x00000001))   n +=  1;

	return n;
}

/**
 * Find the highest order set bit (if any) in a 32 bit value.
 * @note
 *  Big O Log(n) -- Backed by binary bitwise search
 *
 * @param bits
 *  The 32 bit value to scan
 * @return
 *  The bit position of the highest set bit.  -1 if no bits
 */
int m_util_priority_scan_highest(m_uword bits) {

	int n;

	if(bits == 0) {
		return -1;
	}

	n = 0;

	if(bits & 0xFFFF0000) { n += 16; bits >>= 16; }
	if(bits & 0x0000FF00) { n +=  8; bits >>=  8; }
	if(bits & 0x000000F0) { n +=  4; bits >>=  4; }
	if(bits & 0x0000000C) { n +=  2; bits >>=  2; }
	if(bits & 0x00000002)   n +=  1;

	return n;
}

