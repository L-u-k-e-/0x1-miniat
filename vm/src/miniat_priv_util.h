#ifndef _MINIAT_PRIV_UTIL_H_
#define _MINIAT_PRIV_UTIL_H_

#include <stdbool.h>

#include "miniat/miniat.h"
#include "miniat_priv_typedefs.h"

/**
 * Returns a pointer to a newly allocated copy of s.
 * @param m
 *  A miniat
 * @param s
 *  The string to dup
 * @return
 *  NULL on error, or a pointer to the string on success
 */
extern char *m_util_str_duplicate(miniat *m, char *s);

/**
 * Find the lowest order set bit (if any) in a 32 bit value.
 * @note
 *  Big O Log(n) -- Backed by binary bitwise search
 *
 * @param bits
 *  The 32 bit value to scan
 * @return
 *  The bit position of the lowest set bit.  -1 if no bits
 */
extern int m_util_priority_scan_lowest(m_uword bits);

/**
 * Find the highest order set bit (if any) in a 32 bit value.
 * @note
 *  Big O Log(n) -- Backed by binary bitwise search
 *
 * @param bits
 *  The 32 bit value to scan
 * @return
 *  The bit position of the highest set bit.  -1 if no bits
 */
int m_util_priority_scan_highest(m_uword bits);

#endif
